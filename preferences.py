import bpy
from bpy.props import (FloatProperty,
                        BoolProperty,
                        EnumProperty,
                        StringProperty,
                        IntProperty,
                        PointerProperty)

def get_addon_prefs():
    prefs = bpy.context.preferences
    return prefs.addons[__package__].preferences

class myaddonPrefs(bpy.types.AddonPreferences):
    bl_idname = __name__.split('.')[0]

    root_default : bpy.props.StringProperty(
        name='Default Root',
        description="Default root folder path when using export to Zip",
        subtype='DIR_PATH',
        default='')

    use_env_root : BoolProperty(
        name="Use Environement Root",
        description="Use root path specified by environnement keys at startup\nUsing key 'ZIP_ROOT' or 'PROJECT_ROOT' if exists (priority to key ZIP_ROOT if both are set)\nElse always use prefs.",
        default=True,
        )

    def draw(self, context):
            layout = self.layout
            col = layout.column()
            col.label(text='Export to Zip preferences:')
            col.prop(self, 'root_default')
            
            col = layout.column()
            col.label(text='If specified, will be the default root folder path when using export to Zip')
            col.label(text='Typically the root folder of the current project')
            col.prop(self, 'use_env_root')



### --- REGISTER ---

classes=(
myaddonPrefs,
)

def register(): 
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)