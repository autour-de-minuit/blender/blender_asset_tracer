# BAT (blender asset tracer)

Modified version of [Blender's BAT](https://developer.blender.org/source/blender-asset-tracer/browse/master/blender_asset_tracer/) 

Modifications:

- Additional export **Zip pack - keep hierarchy**   
Standard Bat pack is doing an "assembly", modifying file internal links. This zip pack leave the file untouched. Behavior description:  
  - The root folder for zipping use the environment variable `ZIP_ROOT` (accessed with `os.getenv('ZIP_ROOT')` if defined).
  - if `ZIP_ROOT` is not defined, use `PROJECT_ROOT` variable.
  - If there is no such env variables, it fallback to path specified in addon preferences.
  - A checkbox allow to avoid the replacement of root properties with env variables.
  - In the export browser window settings (sidebar), user can choose the packing root folder, prefilled by env or addon preference path
  - If nothing specified, root will be automatically defined with the most upper folder covering the whole dependancies hierarchy.

- Add all UDIM texture in pack


### Changelog

1.3.4

- added: `PROJECT_ROOT` key is used if `ZIP_ROOT` is not specified. Fallback to initial property if neither exists
- added: checkbox to disable env variable check

1.3.3

- code: sys.module name corrected when addon folder isn't named `blender_asset_tracer` as expected

1.3.2

- changed: zip packer ops label `Export File to .ZIP` >> `Zip Pack - keep hierachy`

1.3.1

- added: prefs for file zipper (fallback to this if env is not defined)
- changed: zip operator id_name `bpy.ops.adm.export_zip` >> `bpy.ops.bat.export_zip`